# CookingTiledom

#### 介绍
甜蜜厨房( 三消类微信小游戏 )  + 关卡编辑器( Unity )
![](README_RES/game.gif)

#### 版本
Cocos Creator 2.0~2.4

Unity 2019.x

#### 目录
[Unity_Editor_A_Src: Unity权重关卡编辑器项目跟目录](Unity_Editor_A_Src)

[VirtualEditor: 浏览器访问的可视化关卡编辑器项目跟目录](VirtualEditor)

[CC_Game_Proj: Cocos Creator游戏项目目录](CC_Game_Proj)

[在线体验地址](http://1.117.15.229:8080/)

--------------------------
#### 权重关卡编辑器
通过设置关卡 道具种类 和 权重 配置当前关卡道具
![](README_RES/editor_01.png) ![](README_RES/editor_02.png) 



**效果展示**

![](README_RES/game.gif)

--------------------------
#### 可视化关卡编辑器

![](README_RES/2.gif)
[在线可视化拖拽编辑器 源码](VirtualEditor)

[在线体验地址](www.geek7.ltd)


**效果展示**

![](README_RES/SHOW.gif)

--------------------------
#### 使用说明

1. 为什么选择 Cocos Creator 来开发h5小游戏. 因为这个游戏并不吃性能 所以更多的是需要开发效率. Cocos Creator 的特点就是方便 开发高效
   
2. 为什么选择 Unity来开发编辑器,  你可以通过c++ 纯dx库 + win32 的方式来写一个编辑器 但是无疑会耗费你的大量精力， 一个工具类 更注重的开发效率 代替人工，程序化流程 不是吗?
