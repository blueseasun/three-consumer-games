# CookingTiledom

#### Introduction
Three consumption game ( CocosCreator WeChat mini-game ) + level editor ( Unity )
![](README_RES/game.gif)

#### Version
Cocos Creator 2.0~2.4

Unity 2019.x


#### Weighted Level Editor
Configure the current level props by setting the level prop type and weight
![](README_RES/editor_01.png) ![](README_RES/editor_02.png)


#### Visual level editor
2021.10.13 Under Development.
#### Visual level editor


#### Instructions for use

1. why choose Cocos Creator to develop h5 mini-games. Because this game does not eat performance so it is more about development efficiency. The feature of Cocos Creator is convenient and efficient development
   
2. why choose Unity to develop editor, you can write an editor by c++ pure dx library + win32 way but it will undoubtedly consume a lot of your energy, a tool class more focus on the development efficiency instead of manual, procedural process, right?
